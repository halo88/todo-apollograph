const {gql} = require('apollo-server');

module.exports = gql`
    type Query {
        getAllTasks: [Task]!
    }
    type Mutation {
        addTask(title:String!, content: String!): Task
        editTask(_id: String!, title: String, content: String, status: Boolean): Task
        removeTask(_id: String!): String
    }
    type Task {
        _id: String,
        title: String
        content: String
        status: Boolean
    }
`