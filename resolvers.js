const db = require('./datasource/Database');
const TaskSchema = require('./datasource/dbschema/Task');
const ObjectId = require('mongoose').Types.ObjectId;
const result = {
    Query: {
        getAllTasks: function (parent, args, context, info) {
            return TaskSchema.find().then(
                data => Array.isArray(data) ? data : []
            );
            // console.log(allTasks);
            // return Array.isArray(allTasks) ? allTasks : [];
        }
    },
    Mutation: {
        addTask: function (parent, {title, content}, context, info) {
            console.log("add task " + title);
            newTask = new TaskSchema({
                _id: new ObjectId(),
                title: title,
                content: content,
                status: true
            });
            return newTask.save().then(
                data => data,
                error => {
                    console.log(error);
                    return null;
                }
            );
        },
        editTask: async function (parent, {_id, title, content, status}, context, info) {
            const taskInDb = await TaskSchema.findOne({_id: new ObjectId(_id)});
            taskInDb.title = !!title ? title : taskInDb.title;
            taskInDb.content = !!content ? content : taskInDb.content;
            taskInDb.status = (status == 'undefined' || status == null) ? taskInDb.status : status;
            return taskInDb.save();
        },
        removeTask: async function (parent, {_id}, context, info) {
            try {
                const task = await TaskSchema.findOne({_id: new ObjectId(_id)});
                await task.remove();
                return "removed";
            } catch (e) {
                return "removing failed";
            }
        },
    }
}
module.exports = result;