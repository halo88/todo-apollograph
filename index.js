const {ApolloServer} = require("apollo-server");
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
const server = new ApolloServer({
    typeDefs,
    resolvers,
    // dataSources: {
    // task: require('./datasource/Database.js')
    // }
});
server.listen(1234).then(({url}) => {
    console.log(`server listening at ${url}`);
});