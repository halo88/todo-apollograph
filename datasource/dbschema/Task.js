const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

const Task = new Schema({
    _id: ObjectId,
    title: String,
    content: String,
    status: Boolean,
});
module.exports = mongoose.model('task', Task, 'task');